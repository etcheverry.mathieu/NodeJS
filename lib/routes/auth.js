'use strict';

const Boom = require('boom');
const Joi = require('joi');
const userSchema = require('../schemas/user');

module.exports = [
    {
        method: 'post',
        path: '/auth',
        options: {
            handler: async (request, h) => {
                const { securityService } = request.services();
                const { userService } = request.services();
                let authentif = await userService.authentif(request.payload,securityService);
                if (authentif.length > 0){
                    return h.response('').code(200);
                }
                else {
                    return h.response('').code(404);
                }
            },
            validate:{
                payload:{
                    login: Joi.string().required(), password: Joi.string().min(8).required()
                }
            }, tags : ['api']
        }
    },
];