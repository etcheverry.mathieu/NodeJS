'use strict';

const crypto = require('crypto');

module.exports = class SecurityService {
    async encrypt(password) {
        const passCrypt = await crypto.createHash('sha1').update(password).digest('hex');
        return passCrypt;
    };
};