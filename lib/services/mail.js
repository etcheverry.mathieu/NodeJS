'use strict';

const Mailgen = require('mailgen');
const NodeMailer = require('nodemailer');
const {Service} = require('schmervice');

require('dotenv').config()

const transporteur = NodeMailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.PASSWORD_MAIL_USER
    }
});

const mailGenerator = new Mailgen({
    theme: 'default',
    product: {
        name: 'Mailgen',
        link: 'https://mailgen.js/'
    }
});

module.exports = class MailService extends Service {
    async sendMailCreateUser(userCreated, passwordNotEncrypted) {
        const infoEmail = {
            body: {
                name: userCreated.firstname + ' ' + userCreated.lastname,
                intro: 'Subscription',
                table: {
                    data: [
                        {
                            item: 'User name',
                            information: userCreated.login
                        },
                        {
                            item: 'password',
                            information: passwordNotEncrypted
                        }
                    ],
                    columns: {
                        customWidth: {
                            item: '10%',
                            price: '10%'
                        },
                        customAlignment: {
                            price: 'right'
                        }
                    }
                }
            }
        };

        const bodyMail = mailGenerator.generate(infoEmail);
        const textMail = mailGenerator.generatePlaintext(infoEmail);
        const optionsEnvoi = {
            from: process.env.MAIL_USER,
            to: userCreated.email,
            subject: 'Subscription',
            html: bodyMail,
            text: textMail
        };

        await transporteur.sendMail(optionsEnvoi, (err, info) => {
            if (err) {
                throw (err);
            }
        });
    };

    async sendMailUpdateUser(userUpdated) {
        const infoEmail = {
            body: {
                name: 'Change occured',
                intro: 'Change occured',
                action: {
                    instructions: 'Here',
                    button: {
                        text: 'Here',
                        link: 'http://localhost:3000/documentation'
                    }
                },
            }
        };
        const bodyMail = mailGenerator.generate(infoEmail);
        const textMail = mailGenerator.generatePlaintext(infoEmail);
        const optionsEnvoi = {
            from: process.env.MAIL_USER,
            to: userUpdated.email,
            subject: 'Inscription',
            html: bodyMail,
            text: textMail
        };

        await transporteur.sendMail(optionsEnvoi, (err, info) => {
            if (err) {
                throw (err);
            }
        });
    };

    async sendMailPassword(userUpdated) {
        const infoEmail = {
            body: {
                name: 'Change password',
                intro: 'Change password',
                action: {
                    instructions: 'Here',
                    button: {
                        text: 'Here',
                        link: 'http://localhost:3000/documentation'
                    }
                },
            }
        };
        const bodyMail = mailGenerator.generate(infoEmail);
        const textMail = mailGenerator.generatePlaintext(infoEmail);
        const optionsEnvoi = {
            from: process.env.MAIL_USER,
            to: userUpdated.email,
            subject: '',
            html: bodyMail,
            text: textMail
        };
        await transporteur.sendMail(optionsEnvoi, (err, info) => {
            if (err) {
                throw (err);
            }
        });
    };
};